//Write a program to add two user input numbers using one function.
#include <stdio.h>
int main() {    

    float number1, number2, sum;
    
    printf("Enter number 1:\n");
    scanf("%f", &number1);
    printf("Enter number 2:\n");
    scanf("%f",&number2);
    sum = number1+number2;      
    printf("sum of %f and %f is %f",number1,number2,sum);
    return 0;
}