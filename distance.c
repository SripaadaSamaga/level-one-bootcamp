#include<stdio.h>
#include<math.h>
float point1(float x1, float x2)
{
  return x2 - x1;
}
float point2(float y1, float y2)
{
  return y2 - y1;
}
float distance(float x1,float x2, float y1, float y2)
{
  return sqrt(point1(x1,x2) * point1(x1, x2) + point2(y1, y2) * point2(y1, y2));
}
int main()
{
  float x1, x2, y1, y2;
  printf("enter coordinates of point1 (x1,y1) :\n");
  scanf("%f%f",&x1, &y1);
  printf("enter coordinates of point2 (x2, y2):\n");
  scanf("%f%f", &x2, &y2);
  printf("Distance between (%.2f, %.2f) and (%.2f, %.2f) is %.2f",x1, y1, x2, y2, distance(x1,x2,y1,y2));
  return 0;
}

//WAP to find the distance between two point using 4 functions.